<!--
  ~ This file is part of GNU Taler
  ~ (C) 2020 Taler Systems S.A.
  ~
  ~ GNU Taler is free software; you can redistribute it and/or modify it under the
  ~ terms of the GNU General Public License as published by the Free Software
  ~ Foundation; either version 3, or (at your option) any later version.
  ~
  ~ GNU Taler is distributed in the hope that it will be useful, but WITHOUT ANY
  ~ WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  ~ A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
  ~
  ~ You should have received a copy of the GNU General Public License along with
  ~ GNU Taler; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
  -->

<!-- NOTE: please organize strings in alphabetic order! -->

<resources xmlns:tools="http://schemas.android.com/tools">
    <string name="app_name">Taler Wallet</string>

    <!-- Google Play Store -->

    <string name="google_play_title" tools:keep="@string/google_play_title">Taler Wallet (experimental)</string>
    <string name="google_play_short_desc" tools:keep="@string/google_play_short_desc">GNU Taler makes privacy-friendly online transactions fast and easy.</string>
    <string name="google_play_full_desc" tools:keep="@string/google_play_full_desc"><![CDATA[
This app is a wallet for GNU Taler. It is highly experimental, and not ready for production use yet.

GNU Taler is a privacy-preserving payment system. Customers can stay anonymous, but merchants can not hide their income through payments with GNU Taler. This helps to avoid tax evasion and money laundering.

The primary use case of GNU Taler is payments; it is not meant as a store of value. Payments are always backed by an existing currency.

Payment are made after exchanging existing money into electronic money with the help of an Exchange service, that is, a payment service provider for Taler.

When making a payment, customers only need a charged wallet. A merchant can accept payments without making their customers register on the merchant\'s Website.

GNU Taler is immune against many types of fraud, such as phishing of credit card information or chargeback fraud. In case of loss or theft, only the limited amount of money left in the wallet might be gone.
]]></string>

    <!-- Navigation  -->

    <string name="nav_error">Error</string>
    <string name="nav_exchange_fees">Provider fees</string>
    <string name="nav_exchange_tos">PSP\'s terms of service</string>
    <string name="nav_prompt_withdraw">Withdraw from bank</string>

    <!-- General -->

    <string name="actions">Actions</string>
    <string name="button_back">Go Back</string>
    <string name="button_scan_qr_code">Scan Taler QR code</string>
    <string name="button_scan_qr_code_label">Scan QR code</string>
    <string name="cancel">Cancel</string>
    <!-- The count should be mirrored in RTL languages -->
    <string name="char_count">%1$d/%2$d</string>
    <string name="copy" tools:override="true">Copy</string>
    <string name="currency">Currency</string>
    <string name="edit">Edit</string>
    <string name="enter_uri">Enter taler:// URI</string>
    <string name="enter_uri_label">Enter URI</string>
    <string name="import_db">Import</string>
    <string name="loading">Loading</string>
    <string name="menu">Menu</string>
    <string name="offline">Operation requires internet access. Please ensure your internet connection works and try again.</string>
    <string name="offline_banner">No internet access</string>
    <string name="ok">OK</string>
    <string name="open">Open</string>
    <string name="paste">Paste</string>
    <string name="paste_invalid">Clipboard contains an invalid data type</string>
    <string name="reset">Reset</string>
    <string name="share_payment">Share payment link</string>
    <string name="uri_invalid">Not a valid Taler URI</string>

    <!-- QR code handling -->

    <string name="qr_scan_context_title">Possibly unintended action</string>
    <string name="qr_scan_context_receive_message">Seems like you were intending to receive money, but the QR code that you scanned corresponds to a different action. Do you wish to continue?</string>
    <string name="qr_scan_context_send_message">Seems like you were intending to send money, but the QR code that you scanned corresponds to a different action. Do you wish to continue?</string>

    <!-- Errors -->

    <string name="error_no_uri">Error: No Taler URI was found</string>
    <string name="error_broken_uri">Error: This Taler URI is (currently) not working.</string>
    <string name="error_unsupported_uri">Error: This Taler URI is not supported.</string>

    <!-- Amounts -->

    <string name="amount_available_transfer">Available for transfer: %1$s</string>
    <string name="amount_available_transfer_fees">Available for transfer after fees: %1$s</string>
    <string name="amount_chosen">Chosen amount</string>
    <string name="amount_conversion">Conversion</string>
    <string name="amount_deposit">Amount to deposit</string>
    <string name="amount_effective">Effective amount</string>
    <string name="amount_fee">Fee</string>
    <string name="amount_invalid">Amount invalid</string>
    <string name="amount_invoiced">Amount requested</string>
    <string name="amount_lost">Amount lost</string>
    <string name="amount_negative">-%s</string>
    <string name="amount_max">Maximum amount</string>
    <string name="amount_excess">Amount exceeds maximum of %1$s</string>
    <string name="amount_positive">+%s</string>
    <string name="amount_receive">Amount to receive</string>
    <string name="amount_received">Amount received</string>
    <string name="amount_send">Amount to send</string>
    <string name="amount_sent">Amount sent</string>
    <string name="amount_total">Total amount</string>
    <string name="amount_total_label">Total:</string>
    <string name="amount_transfer">Transfer</string>
    <string name="amount_withdraw">Amount to withdraw</string>

    <!-- Balances -->

    <string name="balance_scope_auditor">Auditor: %1$s</string>
    <string name="balance_scope_exchange">From %1$s</string>
    <string name="balances_empty_state">There is no digital cash in your wallet.\n\nYou can get demo money from the demo bank:\n\nhttps://bank.demo.taler.net</string>
    <string name="balances_empty_demo_url">https://bank.demo.taler.net</string>
    <string name="balances_empty_get_money">Get demo money</string>
    <string name="balances_inbound_amount">+%1$s inbound</string>
    <string name="balances_outbound_amount">-%1$s outbound</string>
    <string name="balances_title">Balances</string>

    <!-- Transactions -->

    <string name="transaction_action_kyc_balance">Complete balance KYC</string>
    <string name="transaction_action_kyc_bank">Complete withdrawal KYC</string>
    <string name="transaction_denom_loss">Loss of funds</string>
    <string name="transaction_deposit">Deposit</string>
    <string name="transaction_deposit_to">Deposit to %1$s</string>
    <string name="transaction_dummy_title">Unknown transaction</string>
    <string name="transaction_order">Purchase</string>
    <string name="transaction_order_id">Order #%1$s</string>
    <string name="transaction_order_total">Total</string>
    <string name="transaction_paid">Paid</string>
    <string name="transaction_peer_pull_credit">Request</string>
    <string name="transaction_peer_pull_debit">Request paid</string>
    <string name="transaction_peer_pull_debit_pending">Paying request</string>
    <string name="transaction_peer_push_credit">Received</string>
    <string name="transaction_peer_push_credit_pending">Receiving</string>
    <string name="transaction_peer_push_debit">Sent</string>
    <string name="transaction_peer_push_debit_pending">Sending</string>
    <string name="transaction_pending">PENDING</string>
    <string name="transaction_preparing_kyc">KYC required, preparing challenge</string>
    <string name="transaction_refresh">Coin expiry change fee</string>
    <string name="transaction_refund">Refund</string>
    <string name="transaction_state_aborted">This transaction was aborted</string>
    <string name="transaction_state_aborted_manual">This transaction was aborted. If you already sent money to this exchange, it will be transferred back to you in %1$s</string>
    <string name="transaction_state_aborting">This transaction is aborting</string>
    <string name="transaction_state_expired">This transaction has expired</string>
    <string name="transaction_state_failed">This transaction has failed or been abandoned</string>
    <string name="transaction_state_pending">This transaction is pending</string>
    <string name="transaction_state_pending_bank">Waiting for authorization in the bank</string>
    <string name="transaction_state_pending_kyc_bank">This transaction would exceed the withdrawal limit set by your bank, in order to continue you must complete KYC verification</string>
    <string name="transaction_state_pending_kyc_balance">This transaction would exceed the limit of balance in your wallet set by the provider, in order to continue you must complete KYC verification</string>
    <string name="transaction_state_suspended">This transaction is suspended</string>
    <string name="transactions_abort">Abort</string>
    <string name="transactions_abort_dialog_message">Are you sure you want to abort this transaction? Funds still in transit might get lost.</string>
    <string name="transactions_abort_dialog_title">Abort transaction</string>
    <string name="transactions_balance">Balance</string>
    <!-- Currency name (symbol) -->
    <string name="transactions_currency">%1$s (%2$s)</string>
    <string name="transactions_delete">Delete</string>
    <string name="transactions_delete_dialog_message">Are you sure you want to remove this transaction from your wallet?</string>
    <string name="transactions_delete_dialog_title">Delete transaction</string>
    <string name="transactions_delete_selected_dialog_message">Are you sure you want to remove the selected transactions from your wallet?</string>
    <string name="transactions_delete_selected_dialog_title">Delete transactions</string>
    <string name="transactions_detail_title">Transaction</string>
    <string name="transactions_empty">You don\'t have any transactions</string>
    <string name="transactions_empty_search">No transactions found. Try a different search.</string>
    <string name="transactions_error">Could not load transactions\n\n%s</string>
    <string name="transactions_fail">Abandon</string>
    <string name="transactions_fail_dialog_message">Are you sure you abandon this transaction? Funds still in transit WILL GET LOST.</string>
    <string name="transactions_fail_dialog_title">Abandon transaction</string>
    <string name="transactions_receive_funds">Receive</string>
    <string name="transactions_resume">Resume</string>
    <string name="transactions_retry">Retry</string>
    <string name="transactions_select_all">Select All</string>
    <string name="transactions_send_funds">Send</string>
    <string name="transactions_suspend">Suspend</string>
    <string name="transactions_title">Transactions</string>

    <!-- Payments -->

    <string name="payment_aborted">Aborted</string>
    <string name="payment_aborting">Aborting</string>
    <string name="payment_already_paid">You\'ve already paid for this purchase.</string>
    <string name="payment_balance_insufficient">Balance insufficient!</string>
    <string name="payment_balance_insufficient_max">Balance insufficient! Maximum is %1$s</string>
    <string name="payment_button_confirm">Confirm payment</string>
    <string name="payment_confirmation_code">Confirmation code</string>
    <string name="payment_create_order">Create order</string>
    <string name="payment_error">Error: %s</string>
    <string name="payment_failed">Failed</string>
    <string name="payment_fee">+%s payment fee</string>
    <string name="payment_initiated">Payment initiated</string>
    <string name="payment_label_order_summary">Purchase</string>
    <string name="payment_pay_template_title">Customize your order</string>
    <string name="payment_pending">Payment not completed, it will be retried</string>
    <string name="payment_prompt_title">Review payment</string>
    <!-- including <amount> <tax name> -->
    <string name="payment_tax">incl. %1$s %2$s</string>
    <string name="payment_template_error">Error creating order</string>
    <string name="payment_title">Payment</string>

    <!-- P2P receive -->

    <string name="receive_peer">Request money from another wallet</string>
    <string name="receive_peer_create_button">Create request</string>
    <string name="receive_peer_invoice_uri">Alternatively, copy and send this URI:</string>
    <string name="receive_peer_payment_instruction">Scan this QR code to pay %1$s</string>
    <string name="receive_peer_payment_intro">Do you want to receive this payment?</string>
    <string name="receive_peer_payment_title">Receive payment</string>
    <string name="receive_peer_title">Request money</string>

    <!-- P2P send -->

    <string name="pay_peer_intro">Do you want to pay this request?</string>
    <string name="pay_peer_title">Pay request</string>
    <string name="send_deposit_account">Account</string>
    <string name="send_deposit_account_add">Add account</string>
    <string name="send_deposit_account_edit">Edit account</string>
    <string name="send_deposit_account_error">It is not possible to deposit to this account, please select another one</string>
    <string name="send_deposit_account_forget_dialog_message">Do you really wish to forget this account in the wallet?</string>
    <string name="send_deposit_account_forget_dialog_title">Forget account</string>
    <string name="send_deposit_account_note">Note</string>
    <string name="send_deposit_account_manage">Manage bank accounts</string>
    <string name="send_deposit_account_warning">You must enter an account that you control, otherwise you will not be able to fulfill the regulatory requirements.</string>
    <string name="send_deposit_bitcoin">Bitcoin</string>
    <string name="send_deposit_bitcoin_address">Bitcoin address</string>
    <string name="send_deposit_bitcoin_create_button">Transfer Bitcoin</string>
    <string name="send_deposit_button_label">Deposit</string>
    <string name="send_deposit_check_fees_button">Check fees</string>
    <string name="send_deposit_create_button">Make deposit</string>
    <string name="send_deposit_host">Local currency bank</string>
    <string name="send_deposit_iban">IBAN</string>
    <string name="send_deposit_iban_error">IBAN is invalid</string>
    <string name="send_deposit_known_bank_accounts">Known bank accounts</string>
    <string name="send_deposit_known_bank_account_delete">Delete</string>
    <string name="send_deposit_known_bank_accounts_empty">No bank accounts saved in the wallet</string>
    <string name="send_deposit_name">Account holder</string>
    <string name="send_deposit_no_methods_error">No supported wire methods</string>
    <string name="send_deposit_select_account_title">Select bank account</string>
    <string name="send_deposit_select_amount_title">Select amount to deposit</string>
    <string name="send_deposit_taler">x-taler-bank</string>
    <string name="send_deposit_title">Deposit to bank account</string>
    <string name="send_deposit_no_alias">No alias</string>
    <string name="send_peer_create_button">Send funds now</string>
    <string name="send_peer_expiration_1d">1 day</string>
    <string name="send_peer_expiration_30d">30 days</string>
    <string name="send_peer_expiration_7d">1 week</string>
    <string name="send_peer_expiration_custom">Custom</string>
    <string name="send_peer_expiration_days">Days</string>
    <string name="send_peer_expiration_hours">Hours</string>
    <string name="send_peer_expiration_period">Expires in</string>
    <string name="send_peer_payment_instruction">Scan this QR code to receive %1$s</string>
    <string name="send_peer_purpose">Purpose</string>
    <string name="send_peer_title">Send money to another wallet</string>

    <!-- Withdrawals -->

    <string name="withdraw_account">Account #%1$d</string>
    <string name="withdraw_account_currency">Account #%1$d (%2$s)</string>
    <string name="withdraw_amount_error">Enter valid amount</string>
    <string name="withdraw_button_confirm">Confirm withdraw</string>
    <string name="withdraw_button_confirm_bank">Authorize in bank</string>
    <string name="withdraw_button_label">Withdraw</string>
    <string name="withdraw_button_tos">Review terms</string>
    <string name="withdraw_cash_acceptor">The amount to be withdrawn will be determined by the cash acceptor</string>
    <string name="withdraw_error_message">Withdrawing is currently not possible. Please try again later!</string>
    <string name="withdraw_error_test">Error withdrawing TESTKUDOS</string>
    <string name="withdraw_error_title">Withdrawal error</string>
    <string name="withdraw_exchange">Provider</string>
    <string name="withdraw_fee">+%1$s withdrawal fees</string>
    <string name="withdraw_initiated">Withdrawal initiated</string>
    <string name="withdraw_manual_bitcoin_intro">Now make a split transaction with the following three outputs.</string>
    <string name="withdraw_manual_qr_epc">EPC QR</string>
    <string name="withdraw_manual_qr_spc">Swiss QR</string>
    <string name="withdraw_manual_ready_account">Account</string>
    <string name="withdraw_manual_ready_bank_button">Open in banking app</string>
    <string name="withdraw_manual_ready_details_intro">Bank transfer details</string>
    <string name="withdraw_manual_ready_iban">IBAN</string>
    <string name="withdraw_manual_ready_intro">To complete the process you need to wire %s to the provider\'s bank account</string>
    <string name="withdraw_manual_ready_receiver">Receiver name</string>
    <string name="withdraw_manual_ready_subject">Subject</string>
    <string name="withdraw_manual_ready_warning">Make sure to use the correct subject, otherwise the money will not arrive in this wallet.</string>
    <string name="withdraw_restrict_age">Restrict usage to age</string>
    <string name="withdraw_restrict_age_unrestricted">Unrestricted</string>
    <string name="withdraw_select_amount">Select amount</string>
    <string name="withdraw_subtitle">Select target bank account</string>
    <string name="withdraw_title">Withdrawal</string>
    <string name="withdraw_waiting_confirm">Waiting for confirmation</string>

    <!-- Exchanges -->

    <string name="exchange_add_error">Could not add provider</string>
    <string name="exchange_add_url">Enter address of provider</string>
    <string name="exchange_delete">Delete provider</string>
    <string name="exchange_delete_force">Force deletion (purge)</string>
    <plurals name="exchange_fee_coin">
        <item quantity="one">Coin: %s (used %d time)</item>
        <item quantity="other">Coin: %s (used %d times)</item>
    </plurals>
    <string name="exchange_fee_coin_expiration_label">Earliest coin expiry:</string>
    <string name="exchange_fee_coin_fees_label">Coin fees</string>
    <string name="exchange_fee_deposit_fee">Deposit fee: %1$s</string>
    <string name="exchange_fee_overhead_label">Rounding loss:</string>
    <string name="exchange_fee_refresh_fee">Change fee: %1$s</string>
    <string name="exchange_fee_refund_fee">Refund fee: %1$s</string>
    <string name="exchange_fee_wire_fee_closing_fee">Closing fee: %1$s</string>
    <string name="exchange_fee_wire_fee_timespan">Timespan: %1$s - %2$s</string>
    <string name="exchange_fee_wire_fee_wire_fee">Wire fee: %1$s</string>
    <string name="exchange_fee_wire_fees_label">Wire fees</string>
    <string name="exchange_fee_withdraw_fee">Withdraw fee: %1$s</string>
    <string name="exchange_fee_withdrawal_fee_label">Withdrawal fee:</string>
    <string name="exchange_global_add">Treat as global</string>
    <string name="exchange_global_add_success">Provider now being treated as global</string>
    <string name="exchange_global_delete">Stop treating as global</string>
    <string name="exchange_global_delete_success">Provider no longer being treated as global</string>
    <string name="exchange_list_add">Add provider</string>
    <string name="exchange_list_add_dev">Add development providers</string>
    <string name="exchange_list_currency">Currency: %1$s</string>
    <string name="exchange_list_empty">No providers known\n\nAdd one manually or withdraw digital cash!</string>
    <string name="exchange_list_error">Could not list providers</string>
    <string name="exchange_list_select">Select provider</string>
    <string name="exchange_list_title">Providers</string>
    <string name="exchange_menu_manual_withdraw">Withdraw</string>
    <string name="exchange_not_contacted">Provider not contacted</string>
    <string name="exchange_reload">Reload information</string>
    <string name="exchange_settings_summary">Manage list of providers known to this wallet</string>
    <string name="exchange_settings_title">Providers</string>
    <string name="exchange_tos_accept">Accept terms of service</string>
    <string name="exchange_tos_forget">Reject terms of service</string>
    <string name="exchange_tos_view">View terms of service</string>
    <string name="exchange_tos_error">Error showing terms of service: %1$s</string>

    <!-- Losses -->

    <string name="loss_reason">Reason</string>
    <string name="loss_reason_expired">Funds were not renewed, because the wallet was not opened for a long time</string>
    <string name="loss_reason_unoffered">The payment provider stopped offering the denomination backing the funds</string>
    <string name="loss_reason_vanished">The payment provider lost the record of the funds</string>

    <!-- Observability -->

    <string name="observability_hide_json">Hide JSON</string>
    <string name="observability_show_json">Show JSON</string>
    <string name="observability_title">Internal event log</string>
    <string name="show_logs">Show logs</string>

    <!-- Settings -->

    <string name="menu_settings">Settings</string>
    <string name="settings_alert_import_canceled">Import cancelled</string>
    <string name="settings_alert_reset_canceled">Reset cancelled</string>
    <string name="settings_alert_reset_done">Wallet has been reset</string>
    <string name="settings_db_clear_error">Error cleaning database</string>
    <string name="settings_db_export">Export database</string>
    <string name="settings_db_export_error">Error exporting database</string>
    <string name="settings_db_export_message">Exporting database, please wait until confirmation</string>
    <string name="settings_db_export_success">Database exported to file</string>
    <string name="settings_db_export_summary">Save internal database</string>
    <string name="settings_db_import">Import database</string>
    <string name="settings_db_import_error">Error importing database</string>
    <string name="settings_db_import_message">Importing database, please wait until confirmation</string>
    <string name="settings_db_import_success">Database imported from file</string>
    <string name="settings_db_import_summary">Restore database from file</string>
    <string name="settings_dev_mode">Developer mode</string>
    <string name="settings_dev_mode_summary">Shows more information intended for debugging</string>
    <string name="settings_dialog_import_message">This operation will overwrite your existing database. Do you want to continue?</string>
    <string name="settings_dialog_reset_message">Do you really want to reset the wallet and lose all coins and purchases?</string>
    <string name="settings_logcat">Debug log</string>
    <string name="settings_logcat_error">Error exporting log</string>
    <string name="settings_logcat_success">Log exported to file</string>
    <string name="settings_logcat_summary">Save internal log</string>
    <string name="settings_reset">Reset Wallet (dangerous!)</string>
    <string name="settings_reset_summary">Throws away your money</string>
    <string name="settings_test">Run integration test</string>
    <string name="settings_test_running">The integration test is now running</string>
    <string name="settings_test_summary">Performs test transactions with demo setup</string>
    <string name="settings_test_withdrawal">Now withdrawing TESTKUDOS</string>
    <string name="settings_version_app">App version</string>
    <string name="settings_version_core">Wallet Core version</string>
    <string name="settings_version_protocol_exchange">Supported Exchange versions</string>
    <string name="settings_version_protocol_merchant">Supported Merchant versions</string>
    <string name="settings_version_unknown">Unknown</string>
    <string name="settings_withdraw_testkudos">Withdraw demo KUDOS</string>
    <string name="settings_withdraw_testkudos_summary">Get money for testing</string>

    <!-- Refunds -->

    <string name="refund_error">Error processing refund</string>
    <string name="refund_success">Refund received!</string>
    <string name="refund_title">Refund</string>

    <!-- Miscellaneous -->

    <string name="host_apdu_service_desc">Taler NFC Payments</string>
    <string name="wifi_connect_error">Could not connect to free Wi-Fi: %1$s</string>
    <string name="wifi_disabled_error">Turn on Wi-Fi to get free Wi-Fi</string>

</resources>
